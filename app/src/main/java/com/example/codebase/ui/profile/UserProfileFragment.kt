package com.example.codebase.ui.profile

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.viewModels
import com.example.codebase.R
import com.example.codebase.base.BaseFragment
import com.example.codebase.data.model.Address
import com.example.codebase.data.model.User
import com.example.codebase.data.room.dao.UserCacheDAO
import com.example.codebase.databinding.FragmentUserProfileBinding
import com.example.codebase.util.isEmailValid
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class UserProfileFragment : BaseFragment<FragmentUserProfileBinding, UserProfileViewModel>() {

    @Inject
    lateinit var userCacheDAO: UserCacheDAO

    companion object {
        fun newInstance(): UserProfileFragment {
            val args = Bundle()
            val fragment = UserProfileFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override val viewModel: UserProfileViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.fragment_user_profile

    override fun initView() {
        viewBinding.btnBack.setOnClickListener {
            parentFragmentManager.popBackStack()
        }

        viewBinding.btnSave.setOnClickListener {
            validateForm()
        }

    }

    override fun setupObserver() {
        userCacheDAO.loadAll().observe(viewLifecycleOwner, {
            it?.get(0)?.let { user ->
                viewModel.user = user
                viewModel.firstName.value = user.firstName ?: ""
                viewModel.lastName.value = user.lastName ?: ""
                viewModel.email.value = user.email ?: ""
                viewModel.dob.value = user.birthDate ?: ""
                viewModel.street.value = user.address?.street ?: ""
                viewModel.streetCode.value = user.address?.streetCode ?: ""
                viewModel.city.value = user.address?.city ?: ""
                viewModel.postalCode.value = user.address?.postalCode ?: ""
                viewModel.country.value = user.address?.country ?: ""
            }
        })

    }

    private fun validateForm() {
        viewModel.apply {
            when {
                (firstName.value.isNullOrEmpty()) -> showToast(getString(R.string.first_name_required))
                (lastName.value.isNullOrEmpty()) -> showToast(getString(R.string.last_name_required))
                (email.value.isNullOrEmpty()) -> showToast(getString(R.string.email_required))
                (!email.value!!.isEmailValid()) -> showToast(getString(R.string.email_invalid))
                (street.value.isNullOrEmpty()) -> showToast(getString(R.string.street_required))
                (streetCode.value.isNullOrEmpty()) -> showToast(getString(R.string.street_code_required))
                (city.value.isNullOrEmpty()) -> showToast(getString(R.string.city_code_required))
                (postalCode.value.isNullOrEmpty()) -> showToast(getString(R.string.postal_code_code_required))
                (country.value.isNullOrEmpty()) -> showToast(getString(R.string.country_code_required))
                else -> {
                    if (user == null) {
                        user = User(firstName.value, lastName.value, email.value, Address(city.value, postalCode.value, street.value, streetCode.value, country.value), dob.value)
                    } else {
                        user?.firstName = firstName.value
                        user?.lastName = lastName.value
                        user?.email = email.value
                        user?.birthDate = dob.value
                        user?.address = Address(city.value, postalCode.value, street.value, streetCode.value, country.value)
                    }
                    userCacheDAO.updateItem(user!!)
                    showToast(getString(R.string.save_changes_completed))
                }
            }
        }
    }

    fun showToast(mess: String) {
        Toast.makeText(requireContext(), mess, Toast.LENGTH_SHORT).show()
    }

}