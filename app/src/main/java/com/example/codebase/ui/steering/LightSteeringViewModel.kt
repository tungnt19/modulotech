package com.example.codebase.ui.steering

import androidx.lifecycle.MutableLiveData
import com.example.codebase.base.BaseViewModel
import com.example.codebase.data.model.Device
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class LightSteeringViewModel @Inject constructor(): BaseViewModel() {

    var device = MutableLiveData<Device>()
    var seekbarValue = MutableLiveData<Int>(50)
    var deviceMode = MutableLiveData<Boolean>(false)
}