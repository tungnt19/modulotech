package com.example.codebase.ui.home

import android.content.res.Configuration
import android.os.Bundle
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.codebase.R
import com.example.codebase.base.BaseFragment
import com.example.codebase.data.model.Device
import com.example.codebase.data.room.dao.DeviceCacheDAO
import com.example.codebase.data.room.dao.UserCacheDAO
import com.example.codebase.databinding.FragmentHomeBinding
import com.example.codebase.ui.MainViewModel
import com.example.codebase.ui.profile.UserProfileFragment
import com.example.codebase.ui.steering.LightSteeringFragment
import com.example.codebase.util.ProductType
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    @Inject
    lateinit var deviceCacheDAO: DeviceCacheDAO

    @Inject
    lateinit var userCacheDAO: UserCacheDAO

    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    val activityViewModel: MainViewModel by activityViewModels()

    override val viewModel: HomeViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.fragment_home

    var listDeviceAdapter: ListDeviceAdapter? = null

    override fun initView() {
        viewBinding.rvListDevice.apply {
            val orientation = resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                layoutManager = GridLayoutManager(requireContext(), 4)
            } else {
                layoutManager = GridLayoutManager(requireContext(), 2)
            }

            if (listDeviceAdapter == null) {
                listDeviceAdapter = ListDeviceAdapter(mutableListOf(), object : ListDeviceAdapter.ListDeviceListener {
                    override fun onClickedItem(item: Device) {
                        parentFragmentManager.beginTransaction().add(R.id.layout_frame, LightSteeringFragment.newInstance(item)).addToBackStack(null).commit()
                    }

                    override fun onRemove(item: Device) {
                        deviceCacheDAO.delete(item)
                    }
                })
            }
            adapter = listDeviceAdapter
        }
        viewBinding.layoutPullToRefresh.setOnRefreshListener {
            activityViewModel.getDevice()
        }
        viewBinding.btnLight.setOnClickListener {
            viewModel.filterLight.postValue(!viewModel.filterLight.value!!)
        }
        viewBinding.btnRoller.setOnClickListener {
            viewModel.filterRoller.postValue(!viewModel.filterRoller.value!!)
        }
        viewBinding.btnHeater.setOnClickListener {
            viewModel.filterHeater.postValue(!viewModel.filterHeater.value!!)
        }

        viewBinding.layoutAvatar.setOnClickListener {
            parentFragmentManager.beginTransaction().add(R.id.layout_frame, UserProfileFragment.newInstance()).addToBackStack(null).commit()

        }
    }

    fun onFilter() {
        var listFilter = mutableListOf<String>()
        if (viewModel.filterLight.value == true) listFilter.add(ProductType.LIGHT)
        if (viewModel.filterRoller.value == true) listFilter.add(ProductType.ROLLER)
        if (viewModel.filterHeater.value == true) listFilter.add(ProductType.HEATER)
        deviceCacheDAO.loadAllData().filter { it.productType in listFilter }.toMutableList().let {
            listDeviceAdapter?.setListDeviceData(it)
        }
    }

    override fun setupObserver() {
        deviceCacheDAO.loadAll().observe(viewLifecycleOwner, {
            viewModel.filterLight.postValue(true)
            viewModel.filterRoller.postValue(true)
            viewModel.filterHeater.postValue(true)
            listDeviceAdapter?.setListDeviceData(it)
            viewBinding.layoutPullToRefresh.isRefreshing = false
        })

        userCacheDAO.loadAll().observe(viewLifecycleOwner, { list ->
            if (!list.isNullOrEmpty()) {
                list[0].let {
                    viewBinding.txtUserName.text = getString(R.string.hello_s, "${it.firstName} ${it.lastName}")
                }
            } else {
                viewBinding.txtUserName.text = getString(R.string.hello_s, "")
            }
        })

        viewModel.filterLight.observe(viewLifecycleOwner, {
            viewBinding.btnLight.setImageResource(if (it) R.drawable.ic_light_selected else R.drawable.ic_light)
            onFilter()
        })

        viewModel.filterRoller.observe(viewLifecycleOwner, {
            viewBinding.btnRoller.setImageResource(if (it) R.drawable.ic_roller_selected else R.drawable.ic_roller)
            onFilter()
        })

        viewModel.filterHeater.observe(viewLifecycleOwner, {
            viewBinding.btnHeater.setImageResource(if (it) R.drawable.ic_heater_selected else R.drawable.ic_heater)
            onFilter()
        })
    }

}