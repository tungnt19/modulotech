package com.example.codebase.ui.profile

import androidx.lifecycle.MutableLiveData
import com.example.codebase.base.BaseViewModel
import com.example.codebase.data.model.User
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class UserProfileViewModel @Inject constructor(): BaseViewModel() {

    var user : User? = null

    var firstName = MutableLiveData<String>()
    var lastName = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var dob = MutableLiveData<String>()
    var street = MutableLiveData<String>()
    var streetCode = MutableLiveData<String>()
    var city = MutableLiveData<String>()
    var postalCode = MutableLiveData<String>()
    var country = MutableLiveData<String>()

}