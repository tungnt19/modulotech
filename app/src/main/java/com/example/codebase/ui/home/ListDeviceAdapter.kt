package com.example.codebase.ui.home

import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.codebase.R
import com.example.codebase.data.model.Device
import com.example.codebase.databinding.ItemDeviceBinding
import com.example.codebase.util.ProductType


class ListDeviceAdapter(var listDevice: MutableList<Device> = mutableListOf(), var listener: ListDeviceListener) :
    RecyclerView.Adapter<ListDeviceAdapter.ViewHolder>() {

    interface ListDeviceListener {
        fun onClickedItem(item: Device)
        fun onRemove(item: Device)
    }

    inner class ViewHolder(val binding: ItemDeviceBinding) : RecyclerView.ViewHolder(binding.root), PopupMenu.OnMenuItemClickListener {
        init {
            binding.btnMenu.setOnClickListener {
                showPopupMenu(it)
            }
        }

        private fun showPopupMenu(view: View) {
            val popupMenu = PopupMenu(view.context, view)
            popupMenu.inflate(R.menu.popup_menu)
            popupMenu.setOnMenuItemClickListener(this)
            popupMenu.show()
        }

        override fun onMenuItemClick(item: MenuItem?): Boolean {
            return when (item!!.itemId) {
                R.id.action_popup_delete -> {
                    listener.onRemove(listDevice[adapterPosition])
                    true
                }
                else -> false
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemDeviceBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val device = listDevice[position]
        holder.binding.txtUserDeviceName.text = device.deviceName
        when (device.productType) {
            ProductType.LIGHT -> holder.binding.image.setImageResource(R.mipmap.image_light)
            ProductType.ROLLER -> holder.binding.image.setImageResource(R.mipmap.image_roller)
            ProductType.HEATER -> holder.binding.image.setImageResource(R.mipmap.image_heater)
            else -> holder.binding.image.setImageResource(R.mipmap.image_light)
        }
        when (device.mode) {
            "ON" -> {
                holder.binding.ivStatus.setImageResource(R.drawable.ic_on)
                holder.binding.ivStatus.visibility = View.VISIBLE
            }
            "OFF" -> {
                holder.binding.ivStatus.setImageResource(R.drawable.ic_off)
                holder.binding.ivStatus.visibility = View.VISIBLE
            }
            else -> {
                holder.binding.ivStatus.visibility = View.GONE
            }
        }

        holder.binding.root.setOnClickListener {
            listener.onClickedItem(device)
        }
    }

    override fun getItemCount(): Int = listDevice.size


    fun setListDeviceData(list: MutableList<Device>?) {
        list?.let {
            this.listDevice.clear()
            this.listDevice.addAll(list)
            notifyDataSetChanged()
        }
    }

}