package com.example.codebase.ui.steering

import android.os.Bundle
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.example.codebase.R
import com.example.codebase.base.BaseFragment
import com.example.codebase.data.model.Device
import com.example.codebase.data.room.dao.DeviceCacheDAO
import com.example.codebase.databinding.FragmentLightSteeringBinding
import com.example.codebase.ui.MainViewModel
import com.example.codebase.util.ProductType
import com.example.codebase.util.gone
import com.example.codebase.util.visible
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LightSteeringFragment : BaseFragment<FragmentLightSteeringBinding, LightSteeringViewModel>() {

    @Inject
    lateinit var deviceCacheDAO: DeviceCacheDAO

    companion object {

        const val SELECTED_DEVICE = "SELECTED_DEVICE"

        fun newInstance(device: Device): LightSteeringFragment {
            val args = Bundle()
            args.putParcelable(SELECTED_DEVICE, device)
            val fragment = LightSteeringFragment()
            fragment.arguments = args

            return fragment
        }
    }

    val activityViewModel: MainViewModel by activityViewModels()

    override val viewModel: LightSteeringViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.fragment_light_steering

    override fun initView() {
        viewModel.device.value = arguments?.getParcelable(SELECTED_DEVICE)!!
        viewBinding.deviceName.text = viewModel.device.value?.deviceName ?: ""
        viewModel.deviceMode.value = viewModel.device.value?.mode == "ON"
        viewBinding.btnBack.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
        when (viewModel.device.value?.productType) {
            ProductType.LIGHT -> {
                viewBinding.sbNum.max = 100
                viewBinding.sbNum.progress = viewModel.device.value?.intensity ?: 0
                viewBinding.txtTitleValue.text = getString(R.string.intensity)
                viewBinding.txtTitleMode.visible()
                viewBinding.btnMode.visible()

            }
            ProductType.ROLLER -> {
                viewBinding.sbNum.max = 100
                viewBinding.sbNum.progress = viewModel.device.value?.position ?: 0
                viewBinding.txtTitleValue.text = getString(R.string.position)
                viewBinding.txtTitleMode.gone()
                viewBinding.btnMode.gone()
            }
            ProductType.HEATER -> {
                viewBinding.sbNum.max = (28 * 2) - (7 * 2)
                viewBinding.sbNum.progress = (viewModel.device.value?.temperature?.minus(7)?.times(2) ?: 0).toInt()
                viewBinding.txtTitleValue.text = getString(R.string.temperature)
                viewBinding.txtTitleMode.visible()
                viewBinding.btnMode.visible()
            }
            else -> {
            }
        }

    }

    override fun setupObserver() {
        viewModel.deviceMode.observe(viewLifecycleOwner, {
            it?.let {
                viewModel.device.value?.mode = if (it) "ON" else "OFF"
                deviceCacheDAO.updateItem(viewModel.device.value!!)
            }
        })
        viewModel.seekbarValue.observe(viewLifecycleOwner, {
            it?.let {
                when (viewModel.device.value?.productType) {
                    ProductType.LIGHT -> {
                        viewModel.device.value?.intensity = it
                    }
                    ProductType.ROLLER -> {
                        viewModel.device.value?.position = it

                    }
                    ProductType.HEATER -> {
                        viewModel.device.value?.temperature = (it.toFloat() / 2 + 7)

                    }
                }
                deviceCacheDAO.updateItem(viewModel.device.value!!)
            }
        })
    }
}