package com.example.codebase.ui

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.codebase.base.BaseViewModel
import com.example.codebase.data.model.Device
import com.example.codebase.data.model.GetDevicesResponse
import com.example.codebase.data.respository.ApiRepository
import com.example.codebase.data.room.dao.DeviceCacheDAO
import com.example.codebase.data.room.dao.UserCacheDAO
import com.example.codebase.util.NetworkHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var apiRepository: ApiRepository

    @Inject
    lateinit var networkHelper: NetworkHelper

    @Inject
    lateinit var deviceCacheDAO: DeviceCacheDAO

    @Inject
    lateinit var userCacheDAO: UserCacheDAO

    fun getDevice() {
        if (networkHelper.isNetworkConnected()) {
            addCompositeDisposable(apiRepository.getDevices().subscribeWith(object : DisposableObserver<GetDevicesResponse>() {
                override fun onNext(value: GetDevicesResponse?) {
                    Log.d("MainViewModel", "$value")
                    deviceCacheDAO.clear()
                    value?.devices?.forEach {
                        deviceCacheDAO.insert(it)
                    }

                    value?.user?.let {
                        userCacheDAO.clear()
                        userCacheDAO.insert(it)
                    }
                }

                override fun onError(e: Throwable?) {
                    e?.printStackTrace()
                }

                override fun onComplete() {
                }

            }))
        }

    }

}