package com.example.codebase.ui

import androidx.activity.viewModels
import com.example.codebase.BR
import com.example.codebase.R
import com.example.codebase.base.BaseActivity
import com.example.codebase.data.room.dao.DeviceCacheDAO
import com.example.codebase.databinding.ActivityMainBinding
import com.example.codebase.ui.home.HomeFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    @Inject
    lateinit var deviceCacheDAO: DeviceCacheDAO

    override val layoutId: Int
        get() = R.layout.activity_main

    override val bindingVariable: Int
        get() = BR.viewModel

    override val viewModel: MainViewModel by viewModels()

    override fun initView() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            supportFragmentManager.beginTransaction().add(R.id.layout_frame, HomeFragment.newInstance()).addToBackStack(null).commit()
            if (deviceCacheDAO.getRowCount() == 0) {
                viewModel.getDevice()
            }
        }
    }

    override fun setupObserver() {
    }

}