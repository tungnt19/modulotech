package com.example.codebase.ui.home

import androidx.lifecycle.MutableLiveData
import com.example.codebase.base.BaseViewModel
import com.example.codebase.data.respository.ApiRepository
import com.example.codebase.util.NetworkHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(): BaseViewModel() {

    @Inject
    lateinit var  apiRepository: ApiRepository

    @Inject
    lateinit var networkHelper: NetworkHelper

    var filterLight = MutableLiveData<Boolean>(true)
    var filterRoller = MutableLiveData<Boolean>(true)
    var filterHeater = MutableLiveData<Boolean>(true)
}