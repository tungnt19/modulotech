package com.example.codebase.util

import androidx.annotation.IntDef
import androidx.annotation.StringDef


class ProductType {

    companion object {
        const val LIGHT = "Light"
        const val ROLLER = "RollerShutter"
        const val HEATER = "Heater"

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(LIGHT, ROLLER, HEATER)
        internal annotation class Type
    }


}