package com.example.codebase.util

import android.annotation.SuppressLint
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.codebase.data.model.Device

class ViewBindingAdapter {
    companion object {
        @SuppressLint("SetTextI18n")
        @BindingAdapter("value", "device")
        @JvmStatic
        fun setTextForSeekValue(view: TextView, value: Int?, device: Device?) {
            value?.let {
                if (device?.productType == ProductType.HEATER) {
                    view.text = (value.toFloat() / 2 + 7).toString()
                } else {
                    view.text = value.toString()
                }
            }
        }
    }
}