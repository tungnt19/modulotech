package com.example.codebase.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.codebase.data.model.Device

@Dao
abstract class DeviceCacheDAO {
    @Insert
    abstract fun insert(device: Device)

    @Update
    abstract fun updateItem(device: Device)

    @Delete
    abstract fun delete(device: Device)

    @Query("SELECT COUNT(id) FROM devicecacheinfo")
    abstract fun getRowCount(): Int

    @Query("DELETE FROM devicecacheinfo")
    abstract fun clear()

    @Query("SELECT * FROM devicecacheinfo")
    abstract fun loadAll(): LiveData<MutableList<Device>>

    @Query("SELECT * FROM devicecacheinfo")
    abstract fun loadAllData(): MutableList<Device>
}