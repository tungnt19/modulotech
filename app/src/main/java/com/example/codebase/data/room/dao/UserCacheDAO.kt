package com.example.codebase.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.codebase.data.model.User

@Dao
abstract class UserCacheDAO {

    @Insert
    abstract fun insert(user: User)

    @Update
    abstract fun updateItem(user: User)

    @Delete
    abstract fun delete(user: User)

    @Query("DELETE FROM usercache")
    abstract fun clear()

    @Query("SELECT * FROM usercache")
    abstract fun loadAll(): LiveData<MutableList<User>>

    @Query("SELECT * FROM usercache")
    abstract fun loadAllData(): MutableList<User>
}