package com.example.codebase.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class Address(
    @Json(name = "city")
    var city: String? = null,

    @Json(name = "postalCode")
    var postalCode: String? = null,

    @Json(name = "street")
    var street: String? = null,

    @Json(name = "streetCode")
    var streetCode: String? = null,

    @Json(name = "country")
    var country: String? = null

) : Parcelable