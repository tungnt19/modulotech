package com.example.codebase.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize


@Parcelize
@Entity(tableName = "usercache")
data class User(

    @Json(name = "firstName")
    var firstName: String? = null,

    @Json(name = "lastName")
    var lastName: String? = null,

    @Json(name = "email")
    var email: String? = null,

    @Json(name = "address")
    var address: Address? = null,

    @Json(name = "birthDate")
    var birthDate: String? = null

) : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var key: Int = 0
}