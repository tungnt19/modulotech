package com.example.codebase.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.codebase.data.model.Device
import com.example.codebase.data.model.User
import com.example.codebase.data.room.converters.Converters
import com.example.codebase.data.room.dao.DeviceCacheDAO
import com.example.codebase.data.room.dao.UserCacheDAO

@Database(
    entities = [Device::class, User::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class CacheDB : RoomDatabase() {
    abstract fun deviceCacheDAO(): DeviceCacheDAO
    abstract fun userCacheDAO(): UserCacheDAO

}