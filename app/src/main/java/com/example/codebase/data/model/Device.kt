package com.example.codebase.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "devicecacheinfo")
data class Device(
    @Json(name = "id")
    var id: String? = null,

    @Json(name = "deviceName")
    var deviceName: String? = null,

    @Json(name = "productType")
    var productType: String? = null,

    @Json(name = "intensity")
     var intensity: Int? = null,

    @Json(name = "position")
     var position: Int? = null,

    @Json(name = "temperature")
     var temperature: Float? = null,

    @Json(name = "mode")
     var mode: String? = null

) : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var key: Int = 0
}