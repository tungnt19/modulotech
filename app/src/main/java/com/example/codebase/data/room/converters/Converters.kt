package com.example.codebase.data.room.converters

import androidx.room.TypeConverter
import com.example.codebase.data.model.Address
import com.example.codebase.data.model.Device
import com.example.codebase.data.model.User
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class Converters {

    var moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @TypeConverter
    fun deviceToString(device: Device): String? {
        return moshi.adapter(Device::class.java).toJson(device)
    }

    @TypeConverter
    fun stringToDevice(json: String): Device? {
        return moshi.adapter(Device::class.java).fromJson(json)

    }

    @TypeConverter
    fun userToString(user: User): String? {
        return moshi.adapter(User::class.java).toJson(user)
    }

    @TypeConverter
    fun stringToUser(json: String): User? {
        return moshi.adapter(User::class.java).fromJson(json)
    }

    @TypeConverter
    fun addressToString(user: Address): String? {
        return moshi.adapter(Address::class.java).toJson(user)
    }

    @TypeConverter
    fun stringToAddress(json: String): Address? {
        return moshi.adapter(Address::class.java).fromJson(json)
    }

}